import init from './app.init.js';
import render from './app.render.js';
import styles from "../../static/css/styles.scss";

let app = (function(){

	let buttonSearch = document.querySelector('button'),
	    insert = document.querySelector('input[type=text]'),
	    letterInserted,
	    isInWord = false;
	
	// Initialize the game with a new word
	let word = init();
	
	console.log(word);

	buttonSearch.addEventListener('click',function(e){
		e.preventDefault();

		//get letter from form
		letterInserted = insert.value;
		
		// check is letter is in word and initialize input text and set focus
		isInWord = word.includes(letterInserted);
		insert.value = '';
		insert.focus();

		//Call render module depending on isInWord value
		if (isInWord){
			render.renderWord(word, letterInserted);
		} else {
			render.renderHangman();
		}
		
	});
})();


