
import config from './app.config.js';

let render = (function(){

    let insert = document.querySelector('input[type=text]'),
        button = document.querySelector('button'),
        hangmanParts = config.hangmanParts,
        wordSection = document.querySelector('section.word'),
        wordBuffer =[],
        failCounter = 0;

    //expose
    let render = {
        createBuffer :createBuffer,
        renderWord : renderWord,
        renderHangman : renderHangman
    };

    //buffer storing matched letters
    function createBuffer(word){
        //Spread-operator
        [... word].forEach(() => {
             wordBuffer.push('_');
        })
       
    };

    function renderWord(word, letter){
        //reset wordsection content
        wordSection.innerHTML='';
        
        //populate the buffer with new matches
        for (let i = 0, len = word.length; i < len; i++) {
            if (letter && (word[i]===letter)){
                wordBuffer[i] = letter;
            }
        };
        // Print out the content of the buffer
        wordBuffer.forEach((value) => {
            let span = document.createElement('span');
            span.innerHTML = value + ' ';
            wordSection.appendChild(span);
        });

        // when buffer is full the game is finished
        if (!wordBuffer.includes('_')){
            alert ('Congratulations!! Your word is: ' + word);
            insert.disabled = true;
            button.disabled = true;
            return
        }
    };
    function renderHangman(){
        // get the element to render
        let spanClass = '.hangman-part-'+failCounter; 
        let partToRender = document.querySelector(spanClass);
        
        partToRender.innerHTML = hangmanParts[failCounter];

        // when the fail counter is equal to hangmanParts.length Game is finished
        failCounter ++;
        if (failCounter === hangmanParts.length){
            alert ("Game Over");
            insert.disabled = true;
            button.disabled = true;
            return
        }
    }

    return render;
})();

export default render;