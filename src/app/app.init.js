
import api from './utils/api.js';
import render from './app.render.js';

let init = (function(){
    let insert = document.querySelector('input[type=text]'),
        button = document.querySelector('button'),
        word;

    //Initialize word and buffer

    function init(){
        insert.removeAttribute("disabled");
        button.removeAttribute("disabled");
        insert.focus();
        word = api.getWord();
	    render.createBuffer(word);
	    render.renderWord(word);

        return word;
    } 
	
    return init;
	
})();

export default init;