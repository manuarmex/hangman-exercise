import config from '../app.config.js';

let api = (function(){

	// This api only get a random word from config file not server calls involved in that case

	let _listOfWords = config.words,
		word = _listOfWords[Math.floor(Math.random() * _listOfWords.length)];

	//expose	
	let api = {
		getWord: getWord
	}
	function getWord(){
		return word;
	}
	
	return api;
})();

export default api;